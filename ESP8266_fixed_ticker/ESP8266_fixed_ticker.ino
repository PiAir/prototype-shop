#include <SPI.h>
#include <Adafruit_GFX.h>
#include <Max72xxPanel.h>

int pinCS = 15; // Attach CS to this pin, DIN to MOSI and CLK to SCK (cf http://arduino.cc/en/Reference/SPI )
int numberOfHorizontalDisplays = 4;
int numberOfVerticalDisplays = 1;

Max72xxPanel matrix = Max72xxPanel(pinCS, numberOfHorizontalDisplays, numberOfVerticalDisplays);

String tape = "-> ESP8266 Scrolling Matrix Demo <-";
int wait = 50; // In milliseconds

int spacer = 1;
int width = 5 + spacer; // The font width is 5 pixels

void setup() {

  matrix.setIntensity(2); // Use a value between 0 and 15 for brightness

// Adjust to your own needs
matrix.setPosition(0, 3, 0); // The first display is at <0, 0>
matrix.setPosition(1, 2, 0); // The second display is at <1, 0>
matrix.setPosition(2, 1, 0); // The third display is at <2, 0>
matrix.setPosition(3, 0, 0); // And the last display is at <3, 0>
//  ...
matrix.setRotation(0, 3);    // The first display is position upside down
matrix.setRotation(1, 3);    // The same hold for the last display
matrix.setRotation(2, 3);    // The same hold for the last display
matrix.setRotation(3, 3);    // The same hold for the last display
}

void loop() {

  for ( int i = 0 ; i < width * tape.length() + matrix.width() - 1 - spacer; i++ ) {

    matrix.fillScreen(LOW);

    int letter = i / width;
    int x = (matrix.width() - 1) - i % width;
    int y = (matrix.height() - 8) / 2; // center the text vertically

    while ( x + width - spacer >= 0 && letter >= 0 ) {
      if ( letter < tape.length() ) {
        matrix.drawChar(x, y, tape[letter], HIGH, LOW, 1);
      }

      letter--;
      x -= width;
    }

    matrix.write(); // Send bitmap to display

    delay(wait);
  }
}

