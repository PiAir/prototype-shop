# README #

Files created during the prototype-shop test-weekends.
See: http://ictoblog.nl/series/prototype-shop

# Notes for the Dot Maxtrix Module #

Libraries needed:
MD_MAX72xx.h - http://arduinocode.codeplex.com/

Add #define USE_FC16_HW 1 into MD_MAX72xx_lib.h !

Use MD_MAX72XX mx = MD_MAX72XX(DATA_PIN, CLK_PIN, CS_PIN, MAX_DEVICES); as hardware interface!

For ESP8266:
Max72xxPanel.h - https://github.com/markruys/arduino-Max72xxPanel

See: https://ictoblog.nl/2016/06/20/getest-dot-matrix-module (in Dutch) for more!


# Notes for .... #

Next post on 27-6-2016