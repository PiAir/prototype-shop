/*
 * Time_NTP.pde
 * Example showing time sync to NTP time source
 *
 * This sketch uses the ESP8266WiFi library
 */

#include <TimeLib.h> 
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <SPI.h>
#include <Adafruit_GFX.h>
#include <Max72xxPanel.h> 

const char ssid[] = "***********";       //  your network SSID (name)
const char pass[] = "***********";       // your network password

int pinCS = 15; // Attach CS to this pin, DIN to MOSI and CLK to SCK (cf http://arduino.cc/en/Reference/SPI )
int numberOfHorizontalDisplays = 4;
int numberOfVerticalDisplays = 1;

Max72xxPanel matrix = Max72xxPanel(pinCS, numberOfHorizontalDisplays, numberOfVerticalDisplays);

String tape = "00:00";
int wait = 50; // In milliseconds

int spacer = 1;
int width = 5 + spacer; // The font width is 5 pixels

// NTP Servers:
IPAddress timeServer(132, 163, 4, 101); // time-a.timefreq.bldrdoc.gov
// IPAddress timeServer(132, 163, 4, 102); // time-b.timefreq.bldrdoc.gov
// IPAddress timeServer(132, 163, 4, 103); // time-c.timefreq.bldrdoc.gov

int dsT = 1; // Daylight saving - TODO calculate automatically!

const int timeZone = 1;     // Central European Time
//const int timeZone = -5;  // Eastern Standard Time (USA)
//const int timeZone = -4;  // Eastern Daylight Time (USA)
//const int timeZone = -8;  // Pacific Standard Time (USA)
//const int timeZone = -7;  // Pacific Daylight Time (USA)


WiFiUDP Udp;
unsigned int localPort = 8888;  // local port to listen for UDP packets

/*-------- NTP code ----------*/

const int NTP_PACKET_SIZE = 48; // NTP time is in the first 48 bytes of message
byte packetBuffer[NTP_PACKET_SIZE]; //buffer to hold incoming & outgoing packets

// send an NTP request to the time server at the given address
void sendNTPpacket(IPAddress &address)
{
  // set all bytes in the buffer to 0
  memset(packetBuffer, 0, NTP_PACKET_SIZE);
  // Initialize values needed to form NTP request
  // (see URL above for details on the packets)
  packetBuffer[0] = 0b11100011;   // LI, Version, Mode
  packetBuffer[1] = 0;     // Stratum, or type of clock
  packetBuffer[2] = 6;     // Polling Interval
  packetBuffer[3] = 0xEC;  // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  packetBuffer[12]  = 49;
  packetBuffer[13]  = 0x4E;
  packetBuffer[14]  = 49;
  packetBuffer[15]  = 52;
  // all NTP fields have been given values, now
  // you can send a packet requesting a timestamp:                 
  Udp.beginPacket(address, 123); //NTP requests are to port 123
  Udp.write(packetBuffer, NTP_PACKET_SIZE);
  Udp.endPacket();
}


time_t getNtpTime()
{
  int received = 0;
  while (Udp.parsePacket() > 0) ; // discard any previously received packets
  while (received == 0) {
    Serial.println("Transmit NTP Request");
    sendNTPpacket(timeServer);
    uint32_t beginWait = millis();
    while (millis() - beginWait < 1500) {
      int size = Udp.parsePacket();
      if (size >= NTP_PACKET_SIZE) {
        Serial.println("Receive NTP Response");
        Udp.read(packetBuffer, NTP_PACKET_SIZE);  // read packet into the buffer
        unsigned long secsSince1900;
        // convert four bytes starting at location 40 to a long integer
        secsSince1900 =  (unsigned long)packetBuffer[40] << 24;
        secsSince1900 |= (unsigned long)packetBuffer[41] << 16;
        secsSince1900 |= (unsigned long)packetBuffer[42] << 8;
        secsSince1900 |= (unsigned long)packetBuffer[43];
        return secsSince1900 - 2208988800UL + timeZone * SECS_PER_HOUR + dsT * SECS_PER_HOUR;
      }
    } 
    Serial.println("No NTP Response :-(");
    delay(2000);
    Serial.println("Retry...");
  }
}

void setup() 
{
  Serial.begin(9600);
  while (!Serial) ; // Needed for Leonardo only
  delay(250);
  Serial.println("TimeNTP Example");
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, pass);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  
  Serial.print("IP number assigned by DHCP is ");
  Serial.println(WiFi.localIP());
  Serial.println("Starting UDP");
  Udp.begin(localPort);
  Serial.print("Local port: ");
  Serial.println(Udp.localPort());
  Serial.println("waiting for sync");
  setSyncProvider(getNtpTime); 

  matrix.setIntensity(2); // Use a value between 0 and 15 for brightness

  // Adjust to your own needs
  matrix.setPosition(0, 3, 0); // The first display is at <0, 0>
  matrix.setPosition(1, 2, 0); // The second display is at <1, 0>
  matrix.setPosition(2, 1, 0); // The third display is at <2, 0>
  matrix.setPosition(3, 0, 0); // And the last display is at <3, 0>
  //  ...
  matrix.setRotation(0, 3);    // The first display is position upside down
  matrix.setRotation(1, 3);    // The same hold for the last display
  matrix.setRotation(2, 3);    // The same hold for the last display
  matrix.setRotation(3, 3);    // The same hold for the last display
}

time_t prevDisplay = 0; // when the digital clock was displayed


void printDigits(int digits){
  // utility for digital clock display: prints preceding colon and leading 0
  Serial.print(":");
  if(digits < 10)
    Serial.print('0');
  Serial.print(digits);
}

void digitalClockDisplay(){
  // digital clock display of the time
  if(day() < 10)
    Serial.print('0');
  Serial.print(day());
  Serial.print("-");
  if(month() < 10)
    Serial.print('0');  
  Serial.print(month());
  Serial.print("-");
  Serial.print(year()); 

  Serial.print(" "); 
  Serial.print(hour());
  printDigits(minute());
  printDigits(second());
  Serial.println(); 
 
}

String digitalClockTape(){

  String DateTimeTape;
  DateTimeTape = "";
  // return te
  if(day() < 10)
    DateTimeTape = DateTimeTape + "0";
  DateTimeTape = DateTimeTape + day() + "-";  
  if(month() < 10)
        DateTimeTape = DateTimeTape + "0";
  DateTimeTape = DateTimeTape + month() + "-";      
  DateTimeTape = DateTimeTape + year() + " ";
  if(hour() < 10)
    DateTimeTape = DateTimeTape + "0";
  DateTimeTape = DateTimeTape + hour() + ":";  
  if(minute() < 10)
        DateTimeTape = DateTimeTape + "0";
  DateTimeTape = DateTimeTape + minute() + "   ";
  Serial.print("DateTimeTape: "); 
  Serial.println(DateTimeTape); 
  return DateTimeTape;
 
}

void loop()
{  
  if (timeStatus() != timeNotSet) {
    if (now() != prevDisplay) { //update the display only if time has changed
      prevDisplay = now();
      digitalClockDisplay();  
    }
    tape = digitalClockTape();
    for ( int i = 0 ; i < width * tape.length() + matrix.width() - 1 - spacer; i++ ) {
      matrix.fillScreen(LOW);

      int letter = i / width;
     int x = (matrix.width() - 1) - i % width;
     int y = (matrix.height() - 8) / 2; // center the text vertically

      while ( x + width - spacer >= 0 && letter >= 0 ) {
       if ( letter < tape.length() ) {
         matrix.drawChar(x, y, tape[letter], HIGH, LOW, 1);
       }

        letter--;
        x -= width;
      }
      matrix.write(); // Send bitmap to display
      delay(wait);
    }
  }
}




